package id.dimas.challenge2

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        openActivityOne()
        openActivityTwo()
    }

    private fun openActivityOne() {
        val buttonOne = findViewById<Button>(R.id.btn_one)
        buttonOne.setOnClickListener {
            val intent = Intent(this, GojekActivity::class.java)
            startActivity(intent)
        }
    }

    private fun openActivityTwo() {
        val buttonTwo = findViewById<Button>(R.id.btn_two)
        buttonTwo.setOnClickListener {
            val intent = Intent(this, ShopeeActivity::class.java)
            startActivity(intent)
        }
    }


}